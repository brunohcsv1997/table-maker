CREATE DATABASE tableMaker;

USE tableMaker;

CREATE TABLE usuarios (
	Id INT PRIMARY KEY AUTO_INCREMENT,
    NickName VARCHAR(50) NOT NULL,
    Gender ENUM('M','F') NOT NULL,
    CreatedAt DATETIME DEFAULT NOW() NOT NULL,
    Email VARCHAR(500) NOT NULL,
    Telephone VARCHAR(30) NOT NULL,
    Birthday DATE DEFAULT NULL
);	

CREATE INDEX IDX_usuarios_NickName ON usuarios(NickName);
CREATE INDEX IDX_usuarios_Gender ON usuarios(Gender);
CREATE INDEX IDX_usuarios_CreatedAt ON usuarios(CreatedAt);
CREATE INDEX IDX_usuarios_Email ON usuarios(Email);
CREATE INDEX IDX_usuarios_Telephone ON usuarios(Telephone);
CREATE INDEX IDX_usuarios_Birthday ON usuarios(Birthday);

INSERT INTO usuarios 
(NickName, Gender, Email, Telephone, Birthday)
VALUES 
('Amanda', 'F', 'amandona@gmail.com', '21992827212', '2001-01-01'),
('Emanuel', 'M', 'MANUEL@yahoomail.com', '49922007212', '1991-05-01'),
('Cristiano', 'M', 'crist@gmail.com', '21992827212', '2002-02-01'),
('Cristiano', 'M', 'crist@gmail.com', '11992827212', '2002-02-01'),
('Ana', 'F', 'analdo@gmail.com', '2122111212', '2008-04-05');

