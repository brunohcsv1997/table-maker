<?php 
	// MySQL
	$inQueryOnlyColumnsRequisitedsByStructure = isset($table['configs']['inQueryOnlyColumnsRequisitedsByStructure']) ? $table['configs']['inQueryOnlyColumnsRequisitedsByStructure'] : 1;
	$distinctsResult = isset($table['configs']['distinctsResult']) ? $table['configs']['distinctsResult'] : 1;
	$queryResultLimitMethod = isset($table['configs']['queryResultLimitMethod']) ? $table['configs']['queryResultLimitMethod'] : 2;
	$queryResultLimit = isset($table['configs']['queryResultLimit']) ? $table['configs']['queryResultLimit'] : 500;
	$applyQueryResultLimit = isset($table['configs']['applyQueryResultLimit']) ? $table['configs']['applyQueryResultLimit'] : 1;

	// Excel Export
	$xlsxExportButton = isset($table['configs']['xlsxExportButton']) ? $table['configs']['xlsxExportButton'] : 1;
	$xlsxName = isset($table['configs']['xlsxName']) ? $table['configs']['xlsxName'] : "xlsx download";
	$xlsxExportButtonText = isset($table['configs']['xlsxExportButtonText']) ? $table['configs']['xlsxExportButtonText'] : "Download";
	$xlsxExportButtonPosition = isset($table['configs']['xlsxExportButtonPosition']) ? $table['configs']['xlsxExportButtonPosition'] : 1;

	// DataTable
	$horizontalScrollDataTable = isset($table['configs']['horizontalScrollDataTable']) ? $table['configs']['horizontalScrollDataTable'] : 0;
	$applyDataTable = isset($table['configs']['applyDataTable']) ? $table['configs']['applyDataTable'] : 0;

	// CSS
	$styleXlsxExportButton = isset($table['configs']['styleXlsxExportButton']) ? $table['configs']['styleXlsxExportButton'] : "width: 10%";
	$styleTable = isset($table['configs']['styleTable']) ? $table['configs']['styleTable'] : "border: 1px solid black; border-radius: 5px; -moz-border-radius: 5px; padding: 5px;border-collapse: separate; border-spacing: 0;";
	$applyStyleOnTable = isset($table['configs']['applyStyleOnTable']) ? $table['configs']['applyStyleOnTable'] : 1;
	$applyStyleOnXlsxExportButton = isset($table['configs']['applyStyleOnXlsxExportButton']) ? $table['configs']['applyStyleOnXlsxExportButton'] : 1;
	$tableNameStyle = isset($table['configs']['tableNameStyle']) ? $table['configs']['tableNameStyle'] : "";

	// Others
	$invertXlsxButtonWithNumRows = isset($table['configs']['invertXlsxButtonWithNumRows']) ? $table['configs']['invertXlsxButtonWithNumRows'] : 0;
	$qtyResults = isset($table['configs']['qtyResults']) ? $table['configs']['qtyResults'] : 1;
	$idTable = isset($table['configs']['idTable']) ? $table['configs']['idTable'] : "table";
	$invisibleTable = isset($table['configs']['invisibleTable']) ? $table['configs']['invisibleTable'] : 0;
	$qtyResultsText = isset($table['configs']['qtyResultsText']) ? $table['configs']['qtyResultsText'] : "((number)) rows in total";
	$loadBootstrap = isset($table['configs']['loadBootstrap']) ? $table['configs']['loadBootstrap'] : 1;
	$tableNameHTagSize = isset($table['configs']['tableNameHTagSize']) ? $table['configs']['tableNameHTagSize'] : 3;
	$classTable = isset($table['configs']['classTable']) ? $table['configs']['classTable'] : "table table-bordered table-striped table-light";
	$classThead = isset($table['configs']['classThead']) ? $table['configs']['classThead'] : "";
	$classXlsxExport = isset($table['configs']['classXlsxExport']) ? $table['configs']['classXlsxExport'] : "btn btn-primary";
	$debug = isset($table['configs']['debugQuery']) ? $table['configs']['debugQuery'] : 0;
	$tfoot = isset($table['configs']['tfoot']) ? $table['configs']['tfoot'] : 0;
	