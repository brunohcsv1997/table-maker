<?php 
	function tableMaker($table) {

		$query = $table['configs']['query'];
	
		include_once "loadTemplateConfig.php";

		if ($debug == 1){
			echo "<h3><font color=yellow>DEBUG QUERY ON</font></h3>";

			echo "<b>original</b>: ". $query;
			echo "<br><br>";
		}

		if ($applyDataTable == 1){
			?>
				<link rel="stylesheet" type="text/css" href="../datatable/media/css/jquery.dataTables.css">
				<style type="text/css" class="init">
				    .dataTables_wrapper .dataTables_info {
				      clear: both;
				      float: left;
				      padding: 5px 20%;
				      position: absolute;
				      top: 0%;
				      text-align: center;
				      pointer-events: none;
				    }

				</style>
				<script type="text/javascript">
				    
				</script>
				<script type="text/javascript" language="javascript" src="../jquery/jquery-3.5.1.js"></script>
				<script type="text/javascript" language="javascript" src="../datatable/media/js/jquery.dataTablesAll.js"></script>
				<script type="text/javascript" language="javascript" src="../datatable/examples/resources/demo.js"></script>
				<script type="text/javascript" language="javascript" class="init">

				    $(document).ready(function() { 
				        // Inicializando o DataTable com length padrão de 50
				        $('#<?php echo $idTable; ?>').DataTable({"pageLength": 50,
				        	<?php
				        		if ($horizontalScrollDataTable == 1){
				        			echo "scrollY: 200,
				        				scrollX: true,";
				        		}
				        	 ?>
				        });
				    } );

				</script>
			<?php 
		}

		if ($inQueryOnlyColumnsRequisitedsByStructure == 1){
			$queryProcessing = "SELECT ";
			foreach ($table['columns'] as $key => $value) {
				if ($key != "configs"){
			    	$queryProcessing .= ",$value";
			    }
			}
			$queryProcessing .= " FROM ($query) resultOnlyConlumns ";
			
			$query = str_replace("SELECT ,", "SELECT ", $queryProcessing);

			if ($debug == 1){
				echo "<b>inQueryOnlyColumnsRequisitedsByStructure</b>: ". $query;
				echo "<br><br>";
			}

		}

		if ($distinctsResult == 1){	
			$query = "SELECT DISTINCT * FROM ($query) resultDistinct ";

			if ($debug == 1){
				echo "<b>distinctsResult</b>: ". $query;
				echo "<br><br>";
			}
		}

		if ($applyQueryResultLimit == 1){
			if ($queryResultLimitMethod == 1){
				$query = $query." LIMIT ". $queryResultLimit;
			} 
			if ($queryResultLimitMethod == 2){
				$query = "SELECT * FROM (".$query.") resultToLimit LIMIT ". $queryResultLimit;
			} 

			if ($debug == 1){
				echo "<b>applyQueryResultLimit[Method ".$queryResultLimitMethod."]</b>: ". $query;
				echo "<br><br>";
			}
		}

		if ($debug == 1){
			echo "<h3><font color=yellow>DEBUG QUERY END</font></h3><br>";
		}

		$result = $table['configs']['conn']->query($query);

		if ($loadBootstrap == 1){
			?>
			<link href="../css/bootstrap.min.css" rel="stylesheet">
			<script src="../js/bootstrap.bundle.min.js"></script>
		<?php }

		echo "<h".$tableNameHTagSize."><font style='".$tableNameStyle."'>".$table['configs']['tableName']."</font></h".$tableNameHTagSize.">";

		if ($invertXlsxButtonWithNumRows == 0){
			if ($qtyResults == 1){
				echo  "<h".$tableNameHTagSize + 1 .">".str_replace("((number))", $result->num_rows , $qtyResultsText)."</h".$tableNameHTagSize + 1 .">"; 
			}

			if ($xlsxExportButton == 1 && $xlsxExportButtonPosition == 1){
				include "../html/xlsxExportButton.html";
				echo "<br>";
			}
			
		} else {
			if ($xlsxExportButton == 1 && $xlsxExportButtonPosition == 1){
				include "../html/xlsxExportButton.html";
				echo "<br>";
			}

			if ($qtyResults == 1){
				echo  "<h".$tableNameHTagSize + 1 .">".str_replace("((number))", $result->num_rows , $qtyResultsText)."</h".$tableNameHTagSize + 1 .">"; 
			}
		}
		
		
		echo "<table id='".$idTable. "' class='".$classTable."' style='";

		if ($applyStyleOnTable == 1){
			echo $styleTable;
		}
		echo "'";

		if ($invisibleTable == 1){
			echo " hidden ";
		}

		echo "><thead class='".$classThead."'>";
		foreach ($table["columns"] as $key => $value) {
			if ($key != "configs"){
		    	echo "<th>$key</th>";
		    }
		}
		echo "</thead><tbody>";


		while($row = $result->fetch_assoc()) {
			echo "<tr>";
			foreach ($table["columns"] as $key => $value) {
				if ($key != "configs"){
			    	echo "<td>$row[$value]</td>";
			    }
			}
			echo "</tr>";
		}

		echo "</tbody>";

		if ($tfoot == 1){
			echo "<tfoot>";
			foreach ($table["columns"] as $key => $value) {
				if ($key != "configs"){
			    	echo "<th>$key</th>";
			    }
			}
			echo "</tfoot>";
		}

		echo "</table>";

		if ($xlsxExportButton == 1 && $xlsxExportButtonPosition == 2){
			include_once "../html/xlsxExportButton.html";
		}

	}