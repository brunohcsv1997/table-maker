<?php 
	$tableUsersFirstExample = 
	array(	
		"columns"=> array(
			// Column Name => Alias/table name on MySQL query
			"Nicky" => "NickName",
		    "E-mail" => "Email",
		    "Gender" => "Gender",
		    "SignedAt" => "CreatedAt",
		    "Birth" => "Birthday",
		),
	    "configs" => array(
	    	"query" => "SELECT * FROM usuarios ORDER BY CreatedAt DESC",
			"tableName" => "Primeiro Exemplo",
			"xlsxName" => "Users",
			"idTable" => "tableUsers",
			"loadBootstrap" => 1,
			"conn" => $conn,
			"applyDataTable" => 1,
		)
	);

	$tableUsersSecondExample = 
	array(	
		"columns"=> array(
			"Apelido" => "NickName",
		    "E-mail" => "Email",
		    "Birth" => "Birthday",
		),
	    "configs" => array(
	    	"query" => "SELECT * FROM usuarios ORDER BY CreatedAt ASC",
			"tableName" => "Segundo Exemplo",
			"xlsxName" => "SecondUsers",
			"idTable" => "tableSecondUsers",
			"conn" => $conn,
			"qtyResults" => 1,
			"classXlsxExport" => 'btn btn-danger',
			"classTable" => "table table-bordered table-striped table-dark",
			"xlsxExportButtonText" => "Baixar",
			"queryResultLimit" => 2,
			"tableNameHTagSize" => 4,
			"styleXlsxExportButton" => "width: 50%",
			"invertXlsxButtonWithNumRows" => 0,
			"qtyResultsText" => "TOTAL DE LINHAS: ((number))",
			"applyDataTable" => 1,
			"horizontalScrollDataTable" => 0,
		)
	);