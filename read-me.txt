Thanks to
*DataTables from: https://datatables.net/
*Table to Excel: https://codepedia.info/javascript-export-html-table-data-to-excel

[PT-BR]
Primeiramente, peço um pouco de paciência, meu código não é tão bom, pois meu foco é BI, dados, etc, mas gosto de elaborar formas rápidas e criativas para visualização de dados.
Table Maker, um montador de tabelas feito em PHP, onde você passa um array com informações de colunas e campos, a query que rodará no MySQL, e algumas configurações personalizadas, então a tabela será montada como desejar, buscando obter um código um pouco mais limpo.